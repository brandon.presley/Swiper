package com.example.swiper

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.widget.Button
import android.widget.TextView
import android.widget.VideoView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import java.io.File

class MainActivity : AppCompatActivity() {
    private var files: List<File>? = null
    private var selected = 0

    @SuppressLint("SetTextI18n")
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val paths = data?.data?.path?.split(":") ?: return@registerForActivityResult
                val path = Environment.getExternalStorageDirectory().path + '/' + paths[1]
                val directory = File(path)
                files = directory.listFiles()?.filter { file -> file.name.endsWith(".webm") }
                val video = findViewById<VideoView>(R.id.videoView)
                video.setVideoURI(files!![0].toUri())
                video.start()
                findViewById<TextView>(R.id.textView).text =
                    "${files!![0].name} (1 / ${files!!.size})"
            }
        }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.pick).setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
            resultLauncher.launch(intent)
        }

        val video = findViewById<VideoView>(R.id.videoView)

        video.setOnPreparedListener {
            it.isLooping = true
        }

        video.setOnClickListener {
            if (video.isPlaying) video.pause()
            else video.start()
        }

        findViewById<Button>(R.id.next).setOnClickListener {
            if (files?.size == null) return@setOnClickListener
            if (selected >= files!!.size - 1) return@setOnClickListener
            selected++
            video.setVideoURI(files!![selected].toUri())
            video.start()
            findViewById<TextView>(R.id.textView).text =
                "${files!![selected].name} (${selected + 1} / ${files!!.size})"
        }

        findViewById<Button>(R.id.prev).setOnClickListener {
            if (files?.size == null) return@setOnClickListener
            if (selected <= 0) return@setOnClickListener
            selected--
            video.setVideoURI(files!![selected].toUri())
            video.start()
            findViewById<TextView>(R.id.textView).text =
                "${files!![selected].name} (${selected + 1} / ${files!!.size})"
        }
    }
}